import {CampaignDescriptor} from "../models/campaign/campaign-descriptor";

export class CampaignDescriptorFactory
{
    private systemType: number;

    constructor(systemType: number)
    {
        this.systemType = systemType;
    }

    public create(): CampaignDescriptor {
        return new CampaignDescriptor(this.systemType);
    }
}
