export class ParticipationType
{
    public static NonParticipant = 0;
    public static CampaignOwner = 1;
    public static CampaignPlayer = 2;
}