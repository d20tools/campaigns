export class SearchSeekType
{
    public static Unknown = 0;
    public static FromStart = 1;
    public static FromEnd = 2;
    public static BeforeDate = 3;
    public static AfterDate = 4;
    public static AroundDate = 5;
}