import {required} from "@treacherous/decorators";

export class CampaignIdRequest
{
    @required()
    public CampaignId = "";
}