import {required} from "@treacherous/decorators";

export class CharacterToken
{
    @required()
    public CharacterPersistedId = "";

    @required()
    public CharacterName = "";
    public Avatar = "";
}