import {required} from "@treacherous/decorators";

export class CampaignInviteToken
{
    public Token = "";
    public DateCreated = new Date();

    @required()
    public Email = "";

    @required()
    public CampaignId = "";
    public CampaignName = "";

    @required()
    public InviterId = "";
    public InviterName = "";
}
