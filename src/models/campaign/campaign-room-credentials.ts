import {maxLength, required} from "@treacherous/decorators";

export class CampaignRoomCredentials
{
    @required()
    public Code = "";

    @maxLength(20)
    public Password = "";
}
