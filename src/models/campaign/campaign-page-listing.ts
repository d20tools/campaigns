export interface CampaignPageListing
{
    PersistedId: string;
    Title: string;
}
