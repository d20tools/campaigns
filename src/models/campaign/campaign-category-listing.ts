import {CampaignPageListing} from "./campaign-page-listing";

export interface CampaignCategoryListing
{
    Category: string;
    Pages: Array<CampaignPageListing>;
}