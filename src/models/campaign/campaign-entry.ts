import {maxLength} from "@treacherous/decorators";

export class CampaignEntry
{
    public Version = "2.0.0";

    public PersistedId = "";
    public CampaignId = "";
    public ChapterId = "";
    public TimelineId = "";

    @maxLength(3000)
    public Content = "";

    public DateCreated = new Date();
    public CreatorAccountId = "";
    public AssociatedCharacterId = "";
    public MetaData = {};
}
