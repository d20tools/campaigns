import {maxLength, minLength, required} from "@treacherous/decorators";

export class CampaignPage
{
    public static LatestVersion = "1.0.0";

    public Version = CampaignPage.LatestVersion;
    public IsSystemPage = false;
    public PersistedId = "";
    public CampaignId = "";

    @required()
    @minLength(2)
    @maxLength(50)
    public Title = "";

    public Content = "";

    @maxLength(50)
    public Category = "";
    public DateCreated = new Date();
    public LastUpdated = new Date();
    public MetaData = {};
}
