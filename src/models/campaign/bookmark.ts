import {maxLength, required} from "@treacherous/decorators";

export class CampaignBookmark
{
    @required()
    @maxLength(50)
    public Name = "";

    @required()
    public TimelineId = "";

    @required()
    public BookmarkEntryId = "";

    public BookmarkDate: Date;
}
