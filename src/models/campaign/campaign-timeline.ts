import {maxLength, required} from "@treacherous/decorators";

export class CampaignTimeline
{
    public TimelineId = "";

    @required()
    @maxLength(30)
    public Name = "";
    public TimelineStart = new Date(0);
    public TimelineFinished: Date = null;
}
