import {required} from "@treacherous/decorators";

export class CampaignInviteResponse
{
    @required()
    public Token = "";
    public Accepted = false;
}
