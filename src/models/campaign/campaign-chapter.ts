import {CampaignArtifact} from "./campaign-artifact";
import {CampaignBookmark} from "./bookmark";
import {CampaignTimeline} from "./campaign-timeline";
import {maxLength, required} from "@treacherous/decorators";

export class CampaignChapter
{
    public static LatestVersion = "2.0.0";

    public Version = CampaignChapter.LatestVersion;

    @required()
    public CampaignId = "";
    public PersistedId = "";

    @required()
    @maxLength(50)
    public Name = "";
    public Artifacts: Array<CampaignArtifact> = [];
    public Bookmarks: Array<CampaignBookmark> = [];
    public Timelines: Array<CampaignTimeline> = [];
    public DateCreated = new Date();
    public IsFinished = false;
    public MetaData = {};
}
