export class DateBasedEntrySearchCriteria
{
    public SeekType: number;
    public ChapterId: string;
    public TimelineId: string;
    public CutoffTime: Date;
    public Count: number;
}
