import {date, maxLength, minLength, number} from "@treacherous/decorators";

export class EntrySearchCriteria
{
    @minLength(3)
    @maxLength(30)
    public ContainingText = "";

    public TimelineId = "";
    public InChapters: Array<string> = [];

    @date()
    public StartTime = "";

    @date()
    public EndTime = "";

    @number()
    public Count = 50;

    @number()
    public StartingIndex = 0;
}
