import {PlayerToken} from "./player-token";
import {CharacterToken} from "./character-token";
import {maxLength, required} from "@treacherous/decorators";

export class CampaignDescriptor
{
    public static LatestVersion = "2.0.0";

    public Version = CampaignDescriptor.LatestVersion;
    public SystemType = 0;
    public PersistedId = 0;

    @required()
    @maxLength(30)
    public Name = "";
    public DateCreated = new Date();
    public LastUpdated = new Date();
    public CreatorAccountId = 0;

    @required()
    public CreatorName = "";
    public Players: Array<PlayerToken> = [];
    public Characters: Array<CharacterToken> = [];
    public MetaData = {};

    constructor(systemType: number)
    {
        this.SystemType = systemType;
    }
}
