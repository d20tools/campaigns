import {required} from "@treacherous/decorators";

export class PlayerToken
{
    @required()
    public AccountId = "";
    public CharacterPersistedId = "";

    @required()
    public DisplayName = "";
    public AvatarHash = "";
    public LastOnlineCheck = new Date();
}
