import {ICharacter} from "@d20tools/characters";
import {PlayerToken} from "./player-token";

export class PlayerCharacter
{
    public Character: ICharacter;
    public Player: PlayerToken;

    constructor(character: ICharacter, playerToken: PlayerToken)
    {
        this.Character = character;
        this.Player = playerToken;
    }
}
