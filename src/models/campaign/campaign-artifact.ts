import {maxLength, required, withRule} from "@treacherous/decorators";

export class CampaignArtifact
{
    public Id = 0;

    @required()
    @maxLength(30)
    public Name = "";

    @maxLength(1000)
    public Description = "";

    @withRule("isImageUrl")
    public Url = "";

    public DateCreated = new Date();
}
